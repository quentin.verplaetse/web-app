module.exports = {
    local: 'http://localhost:3000/',
    prod: 'https://web-app-tek-prod.herokuapp.com/',
    qa: 'https://web-app-tek.herokuapp.com/'
}